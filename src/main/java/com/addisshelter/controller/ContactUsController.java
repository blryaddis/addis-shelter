package com.addisshelter.controller;
	
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


	@Controller        
	public class ContactUsController {
	  
	  @GetMapping("/home/contactus")
	  public String home() {
	    return "home/contactus";    
	  }

	}
