package com.addisshelter.controller;

import org.springframework.web.bind.annotation.GetMapping;

public class HomeController {

	@GetMapping("/home/home")
	  public String home() {
		 // throw new RuntimeException("Test Exception");
	    return "home/home";    
	  }
	  
	  @GetMapping("/testsec")
	  public String testSec() {
	    return "testsec";    
	  }
}
