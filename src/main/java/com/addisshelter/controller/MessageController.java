package com.addisshelter.controller;

import java.util.List;

//import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

//import java.util.List;

//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.servlet.ModelAndView;

import com.addisshelter.domain.Message;
//import com.addisshelter.service.MessageService;
import com.addisshelter.service.MessageServiceImpl;
//import com.addisshelter.domain.Message;
//import com.addisshelter.service.MessageService;

@Controller

public class MessageController {
	@Autowired
	MessageServiceImpl messageService;
	   
	@ModelAttribute("message")
	public Message newMess(Model model) {
		return new Message();
	}
	@ModelAttribute("messages")
	public List<Message> Mess(Model model) {
		return messageService.getAllMessage1();
	}
	@GetMapping("home/message")
    public String greetingForm() {
        return "home/message";
    }
	@PostMapping("home/message")
	public String addMessage(Message message) {
		messageService.saveOrUpdate1(message);
		return "home/message";
	}
	}
    		


