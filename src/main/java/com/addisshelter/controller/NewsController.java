package com.addisshelter.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import com.addisshelter.domain.News;
import com.addisshelter.service.NewsService;
import com.addisshelter.service.NewsServiceImpl;

@Controller

public class NewsController {
	@Autowired
	NewsServiceImpl newsService;
	   
	@ModelAttribute("news")
	public News newMess(Model model) {
		return new News();
	}
	@ModelAttribute("newss")
	public List<News> Mess(Model model) {
		return NewsService.getAllNews1();
	}
	@GetMapping("home/news")
  public String greetingForm() {
      return "home/news";
  }
	@PostMapping("home/news")
	public String addMessage(News news) {
		newsService.saveOrUpdate1(news);
		return "home/news";
	}
	}
