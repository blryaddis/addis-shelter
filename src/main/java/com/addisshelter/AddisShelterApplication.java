package com.addisshelter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AddisShelterApplication {

	public static void main(String[] args) {
		SpringApplication.run(AddisShelterApplication.class, args);
	}

}
