package com.addisshelter.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;




import com.addisshelter.security.User;


@Entity
@Table(name="user")
public class Registration {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@PrePersist
	void placedAt() {
		new Date();
	}	
	@Column(name = "typeofcondominium")
    @NotBlank(message = "Please provide typeofcondominium")
    private String typeofcondominium;
	
	@Column(name = "firstname")
    @NotBlank(message = "Please provide your first name")
    private String firstname;
    
    @Column(name = "lastname")
    @NotBlank(message = "Please provide your last name")
    private String lastname;
  
    @Column(name = "age")
    @NotBlank(message = "Please provide your age")
    private String age;
	
    @Column(name = "sex")
    @NotBlank(message = "Please provide your sex")
    private String sex;
    
    @Column(name = "subcity")
    @NotBlank(message = "Please provide your sub city")
    private String subcity;
    
    @Column(name = "woreda")
    @NotBlank(message = "Please provide your woreda")
    private String woreda;
	
    @Column(name = "phonenumber")
    @NotBlank(message = "Please provide your phone number")
    private String phonenumber;
    
    @Column(name = "housenumber")
    @NotBlank(message = "Please provide your house number")
    private String housenumber;
    
    @Column(name = "merriageststus")
    @NotBlank(message = "Please provide your merriage status")
    private String merriageststus;
    
    @Column(name = "registrationdate")
    @NotBlank(message = "Please provide your registration date")
    private String registrationdate;
    
    @Column(name = "numberofbedroom")
    @NotBlank(message = "Please provide your number of bedroom")
    private String numberofbedroom;
	
    @ManyToOne
	private  User user;

}
