package com.addisshelter.security;

import java.util.Collection;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "user")
public class User implements UserDetails  {
 
 @Id
 @GeneratedValue(strategy = GenerationType.AUTO)
 private int id;
 
 @Column(name = "email")
 private String email;
 
 @Column(name = "typeofcondominium")
 private String typeofcondominium;
 
 @Column(name = "firstname")
 private String firstname; 
 
 @Column(name = "lastname")
 private String lastname;
 
 @Column(name = "age")
 private String age;
 
 @Column(name = "sex")
 private String sex;
 
 @Column(name = "subcity")
 private String subcity;
 
 @Column(name = "woreda")
 private String woreda;
 
 @Column(name = "phonenumber")
 private String phonenumber;
 
 @Column(name = "housenumber")
 private String housenumber;
 
 @Column(name = "merriagestatus")
 private String merriagestatus;
 
 @Column(name = "registrationdate")
 private String registrationdate;
 
 @Column(name = "numberofbedroom")
 private String numberofbedroom;
 
 @Column(name = "password")
 private String password;
 
 @Column(name = "active")
 private int active;
 //
 //
 //
 //
 //
 //
 @ManyToMany(cascade=CascadeType.ALL)
 @JoinTable(name="user_role", joinColumns=@JoinColumn(name="user_id"), inverseJoinColumns=@JoinColumn(name="role_id"))
 private Set<Role> roles;

 public int getId() {
  return id;
 }

 public void setId(int id) {
  this.id = id;
 }

 public String getEmail() {
  return email;
 }

 public void setEmail(String email) {
  this.email = email;
 }
 
 public String getTypeofcondominium() {
  return typeofcondominium;
}

public void setTypeofcondominium(String typeofcondominium) {
	this.typeofcondominium = typeofcondominium;
 }
 public String getFirstname() {
  return firstname;
 }

 public void setFirstname(String firstname) {
  this.firstname = firstname;
 }

 public String getLastname() {
  return lastname;
 }

 public void setLastname(String lastname) {
  this.lastname = lastname;
 }
 
 public String getAge() {
	  return age;
	 }

public void setAge(String age) {
	this.age = age;
	 }
public String getSex() {
	return sex;
		 }
 public void setSex(String sex) {
	this.sex = sex;
		 }
public String getSubcity() {
	 return subcity;
			 }
 public void setSubcity(String subcity) {
	this.subcity = subcity;
			 }
public String getWoreda() {
	return woreda;
				 }
public void setWoreda(String woreda) {
	this.woreda = woreda;
				 }
public String getPhonenumber() {
	return phonenumber;
				 }
public void setPhonenumber(String phonenumber) {
	this.phonenumber = phonenumber;
				 }
public String getHousenumber() {
	return housenumber;
				 }
public void setHousenumber(String housenumber) {
	this.housenumber = housenumber;
				 }
public String getMerriagestatus() {
	return merriagestatus;
				 }
public void setMerriagestatus(String merriagestatus) {
	this.merriagestatus = merriagestatus;
				 }
public String getRegistrationdate() {
	return registrationdate;
				 }
public void setRegistrationdate(String registrationdate) {
	this.registrationdate = registrationdate;
				 }
public String getNumberofbedroom() {
	return numberofbedroom;
				 }
public void setNumberofbedroom(String numberofbedroom) {
	this.numberofbedroom = numberofbedroom;
				 }
 public String getPassword() {
  return password;
 }

 public void setPassword(String password) {
  this.password = password;
 }

 public int getActive() {
  return active;
 }

 public void setActive(int active) {
  this.active = active;
 }

 public Set<Role> getRoles() {
  return roles;
 }

 public void setRoles(Set<Role> roles) {
  this.roles = roles;
 }

@Override
public Collection<? extends GrantedAuthority> getAuthorities() {
	return null;
}

@Override
public String getUsername() {
	return null;
}

@Override
public boolean isAccountNonExpired() {
	return false;
}

@Override
public boolean isAccountNonLocked() {
	return false;
}

@Override
public boolean isCredentialsNonExpired() {
	return false;
}

@Override
public boolean isEnabled() {
	return false;
}
}