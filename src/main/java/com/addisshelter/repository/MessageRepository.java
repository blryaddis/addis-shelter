package com.addisshelter.repository;


import org.springframework.data.repository.CrudRepository;

import com.addisshelter.domain.Message;


public interface MessageRepository extends CrudRepository <Message, Long> {


}
