package com.addisshelter.repository;

import org.springframework.data.repository.CrudRepository;

import com.addisshelter.domain.News;


public interface NewsRepository extends CrudRepository <News, Long> {


}