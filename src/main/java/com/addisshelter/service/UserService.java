package com.addisshelter.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.addisshelter.domain.Message;
import com.addisshelter.security.User;

public interface UserService extends UserDetailsService{
  
 public User findUserByEmail(String email);
 
 public void saveUser(User user);

public Message findMessageByName(String name);

public void saveMessage(Message message);
}
