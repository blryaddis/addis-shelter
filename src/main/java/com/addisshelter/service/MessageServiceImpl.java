package com.addisshelter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.addisshelter.domain.Message;
import com.addisshelter.repository.MessageRepository;

@Service
@Transactional
public class MessageServiceImpl implements MessageService{
	
@Autowired	
MessageRepository messageRepository;
@Override
public List<Message> getAllMessage() {
	
	return (List<Message>) messageRepository.findAll();
}
@Override
public Message getMessageById1(long id) {
	
	return messageRepository.findById(id).get();
}

@Override
public void saveOrUpdate1(Message message) {
	messageRepository.save(message);
	
	
}
@Override
public void deleteMessage1(long id) {
	messageRepository.deleteById(id);
}

@Override
public List<Message> getAllMessage1() {
	// TODO Auto-generated method stub
	return null;
}
@Override
public Message getMessageById(long id) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public void deleteMessage(long id) {
	// TODO Auto-generated method stub
	
}


}
