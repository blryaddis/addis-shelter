package com.addisshelter;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

public class HomePageTest {
	@Autowired
	  private MockMvc mockMvc;   // <2>

	  @Test
	  public void contextLoads() throws Exception {
	    mockMvc.perform(get("home/home"))    // <3>
	    
	      .andExpect(status().isOk())  // <4>
	      
	      .andExpect(view().name("home/home"));  // <5>
	      
	      }
}
