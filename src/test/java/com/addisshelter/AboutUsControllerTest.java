package com.addisshelter;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;



@RunWith(SpringRunner.class)

@SpringBootTest
public class AboutUsControllerTest {

  @Autowired
  private MockMvc mockMvc;   // <2>

  @Test
  public void testAboutUs() throws Exception {
    mockMvc.perform(get("/home/aboutus"))    // <3>
    
      .andExpect(status().isOk())  // <4>
      
      .andExpect(view().name("home/aboutus"));  // <5>
      
      }

}

