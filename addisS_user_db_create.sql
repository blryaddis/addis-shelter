
    create table hibernate_sequence (
       next_val bigint
    ) engine=InnoDB

    insert into hibernate_sequence values ( 1 )

    insert into hibernate_sequence values ( 1 )

    create table role (
       role_id integer not null,
        role varchar(255),
        primary key (role_id)
    ) engine=InnoDB

    create table user (
       id integer not null,
        active integer,
        age varchar(255),
        email varchar(255),
        firstname varchar(255),
        housenumber varchar(255),
        lastname varchar(255),
        merriagestatus varchar(255),
        numberofbedroom varchar(255),
        password varchar(255),
        phonenumber varchar(255),
        registrationdate varchar(255),
        sex varchar(255),
        subcity varchar(255),
        typeofcondominium varchar(255),
        woreda varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id integer not null,
        role_id integer not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (role_id)

    alter table user_role 
       add constraint FK859n2jvi8ivhui0rl0esws6o 
       foreign key (user_id) 
       references user (id)
